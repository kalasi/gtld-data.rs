extern crate gtld_data;

use gtld_data::{GtldKind,
            Gtld,
            all,
            domain_exists,
            get_by_kind,
            get_by_organization};

#[test]
fn test_all() {
    // Test the number of gtlds for bc purposes.
    let all: Vec<Gtld> = all();

    // The number all.len() is being tested against _can_ change without a bc
    // break. The following rules are considered:
    //
    // - If the gTLD is an addition (new), this number can be incremented
    //   by only increasing the PATCH version (x.y.2) -> (x.y.3)
    // - If the gTLD is a removal (no longer exists), this number can be
    //   decremented by increasing the MAJOR version (1.y.z) -> (2.0.0)
    assert!(all.len() > 1000);
}

#[test]
fn test_gtldkind() {
    // Ensure that all the enum values exist for bc purposes.
    let _ = GtldKind::CountryCode;
    let _ = GtldKind::GenericRestricted;
    let _ = GtldKind::Generic;
    let _ = GtldKind::Infrastructure;
    let _ = GtldKind::Sponsored;
    let _ = GtldKind::Test;
}

#[test]
fn test_domain_exists() {
    // Test that a domain does exist. `com` should always exist.
    assert!(domain_exists("com"));

    // Test that a domain doesn't exist. `aaaaaa` will probably never exist.
    // But considering all the new gTLDs, who knows?
    assert!(!domain_exists("aaaaaa"));

    // Test that passing a String works.
    assert!(domain_exists(String::from("com")));
}

#[test]
fn test_get_by_kind() {
    // Retrieving by each `GtldKind`s returns a specific number of `Gtld`s.
    // This is also useful for testing a bc break.
    assert!(get_by_kind(GtldKind::CountryCode).len() > 0);
    assert!(get_by_kind(GtldKind::GenericRestricted).len() > 0);
    assert!(get_by_kind(GtldKind::Generic).len() > 500);
    assert!(get_by_kind(GtldKind::Infrastructure).len() > 0);
    assert!(get_by_kind(GtldKind::Sponsored).len() > 0);
    assert!(get_by_kind(GtldKind::Test).len() > 0);
}

#[test]
fn test_get_by_organization() {
    // These are some randomly selected organizations and the selections don't
    // mean anything. This is just a way to make sure that it's working
    // correctly and always returning the right number of `Gtld`s.
    let o1 = "VeriSign Global Registry Services";
    let o2 = "Amazon Registry Services, Inc.";
    let o3 = "Saudi Telecom Company";

    assert!(get_by_organization(o1).len() > 0);
    assert!(get_by_organization(o2).len() > 0);
    assert!(get_by_organization(o3).len() > 0);

    // Test that passing a `String` works.
    assert!(get_by_organization(String::from(o1)).len() > 0);
}
