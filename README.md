[travis-badge]: https://img.shields.io/travis/taiyaeix/gtld-data.rs.svg?style=flat-square
[travis]: https://travis-ci.org/taiyaeix/gtld-data.rs
[license-badge]: https://img.shields.io/badge/license-ISC-blue.svg?style=flat-square
[license]: https://opensource.org/licenses/ISC

[![travis-badge][]][travis] [![license-badge][]][license]

# gtld-data.rs

Rust crate for gTLD data with a few useful functions. The list of gTLDs is
created/updated on build of the library.

All renames, additions, and removals of gTLD's are due to the IANA removing
their entries.

All API changes are subject to Semver.


### gTLD

> A Generic Top Level Domain (gTLD) is an internet domain name extension with
> three or more characters. It is one of the categories of the top level
> domain (TLD) in the Domain Name System (DNS) maintained by the Internet
> Assigned Numbers Authority.
>
> -- [icannwiki](http://icannwiki.com/GTLD)


### Installation

Add the following dependency to your Cargo.toml:

```rust
gtld-data = "^0.4"
```

And include it in your project:

```rust
extern crate gtld_data;
```

### Examples

Retrieve a `Vec` of all `Gtld` definitions:

```rust
let gtlds = gtld_data::all();
```


Check if a domain for a `Gtld` exists:

```rust
use gtld_data::domain_exists;

let exists = domain_exists("com");
```


Retrieve a `Vec` of `Gtld`s based on a given `GtldKind`:

```rust
use gtld_data::{GtldKind, get_by_kind};

let generics = get_by_kind(GtldKind::Generic);

assert!(generics.len() > 0);
```


Retrieve a `Vec` of `Gtld`s based on the name of the organization passed:

```rust
use gtld_data::get_by_organization;

let organization = "VeriSign Global Registry Services";
let gtlds = get_by_organization(organization);

assert!(gtlds.len() > 0);
```


### License

License info in [LICENSE.md]. Long story short, ISC.

[LICENSE.md]: https://gitlab.com/kalasi/gtld-data.rs/blob/master/LICENSE.md
