// ISC License (ISC)
//
// Copyright (c) 2016, Austin Hellyer <hello@austinhellyer.me>
//
// Permission to use, copy, modify, and/or distribute this software for any
// purpose with or without fee is hereby granted, provided that the above
// copyright notice and this permission notice appear in all copies.
//
// THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES
// WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF
// MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY
// SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES WHATSOEVER
// RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN ACTION OF
// CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF OR IN
// CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.
//! # gtld-data
//!
//! A crate for retrieving gTLDs of all types.
//!
//! ## Installation
//!
//! Add the dependency to your project's `Cargo.toml`:
//!
//! ```toml
//! [dependencies]
//! gtld-data = "^0.4"
//! ```
//!
//! and add it to your project:
//!
//! ```rust
//! extern crate gtld_data;
//! ```
//!
//! ## What is a gtLD?
//!
//! | Generic Top Level Domain (gTLD) is an internet domain name extension with
//! | three or more characters. It is one of the categories of the top level
//! | domain (TLD) in the Domain Name System (DNS) maintained by the Internet
//! | Assigned Numbers Authority.
//! |
//! | - [icannwiki](http://icannwiki.com/GTLD)

pub mod gtlds;

// Re-export the `all` method since there's no reason to make userland go
// through the `gtlds` mod.
pub use gtlds::all;

/// The type of what a `Gtld` is. Each `Gtld` has a different type, with each
/// having a different purpose.
///
/// Wikipedia has a good summary of the differences of each:
///
/// <https://en.wikipedia.org/wiki/Top-level_domain#Types>
#[derive(Clone, Copy, Debug, PartialEq)]
pub enum GtldKind {
    /// *ccTLD*: Two-letter domains established for countries or territories.
    CountryCode,
    /// *grTLD*: domains managed under official ICANN accredited registrars.
    GenericRestricted,
    /// *gTLD*: Top-level domains with three or more characters.
    Generic,
    /// *ARPA*: Consists of one domain, the Address and Routing Parameter Area.
    Infrastructure,
    /// Domains that are proposed and sponsored by private agencies or
    /// organizations.
    Sponsored,
    /// Domains installed under .test for testing purposes in the IDN
    /// development process.
    Test,
}

/// Representation of each `Gtld` with its three key pieces of data:
///
/// - *domain*: `str` of the domain name for this `Gtld`, such as `com` or `pw`.
/// - *kind*: `GtldKind` enum value of the type of `Gtld` that this is. For
///           technical purposes, this is renamed from `type` to `kind`.
/// - *organization*: `str` of the name of the organization that represents this
///                   `Gtld`.
#[derive(Clone, Debug)]
pub struct Gtld<'a> {
    pub domain: &'a str,
    pub kind: GtldKind,
    pub organization: &'a str,
}

/// Determines whether a domain exists within a `Gtld` defined by the
/// `gtld_data::all()` method.
///
/// # Examples
///
/// Check if a domain exists given a `str`:
///
/// ```rust
/// use gtld_data::domain_exists;
///
/// let exists = domain_exists("com");
/// ```
///
///
/// Check if a domain exists given a `String`:
///
/// ```rust
/// use gtld_data::domain_exists;
///
/// let exists = domain_exists(String::from("net"));
/// ```
pub fn domain_exists<S: Into<String>>(domain: S) -> bool {
    let domain_str: &str = &domain.into()[..];

    for gtld in all() {
        if gtld.domain == domain_str {
            return true;
        }
    }

    false
}

/// Retrieves all `Gtld`s defined by the `gtld_data::all()` method given a
/// `GtldKind` enum value. For example, passing a `GtldKind::Generic` will
/// return all `Gtld`s that have a `kind` value of `GtldKind::Generic`.
///
/// # Examples
///
/// Retrieve all `Gtld`s with a `kind` of `GtldKind::Generic`:
///
/// ```rust
/// use gtld_data::{GtldKind, get_by_kind};
///
/// let generics = get_by_kind(GtldKind::Generic);
///
/// assert!(generics.len() > 0);
/// ```
///
/// Alternatively the same can be done for all values of `GtldKind`, for
/// example:
///
/// ```rust
/// use gtld_data::{GtldKind, get_by_kind};
///
/// let sponsored = get_by_kind(GtldKind::Sponsored);
///
/// assert!(sponsored.len() > 0);
/// ```
pub fn get_by_kind<'a>(kind: GtldKind) -> Vec<Gtld<'a>> {
    all().into_iter()
        .filter(|g| g.kind == kind)
        .collect()
}

/// Retrieves all `Gtld`s defined by the `gtld_data::all()` method with an
/// `organization` name equal to that of the `organization` passed.
///
/// # Examples
///
/// Retrieve all `Gtld`s represented by the `str`
/// "VeriSign Global Registry Services":
///
/// ```rust
/// use gtld_data::get_by_organization;
///
/// let organization = "VeriSign Global Registry Services";
/// let gtld_data = get_by_organization(organization);
///
/// assert!(gtld_data.len() > 0);
/// ```
///
/// Alternatively, pass a `String` of the organization name:
///
/// ```rust
/// use gtld_data::get_by_organization;
///
/// let organization = String::from("VeriSign Global Registry Services");
/// let gtld_data = get_by_organization(organization);
///
/// assert!(gtld_data.len() > 0);
/// ```
pub fn get_by_organization<'a, S: Into<String>>(organization: S)
    -> Vec<Gtld<'a>> {
    let org_str: &str = &organization.into()[..];

    all().into_iter()
        .filter(|g| g.organization == org_str)
        .collect()
}
