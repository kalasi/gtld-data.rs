// ISC License (ISC)
//
// Copyright (c) 2016, Austin Hellyer <hello@austinhellyer.me>
//
// Permission to use, copy, modify, and/or distribute this software for any
// purpose with or without fee is hereby granted, provided that the above
// copyright notice and this permission notice appear in all copies.
//
// THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES
// WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF
// MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY
// SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES WHATSOEVER
// RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN ACTION OF
// CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF OR IN
// CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.
//
// What is a gtLD?
//
// | Generic Top Level Domain (gTLD) is an internet domain name extension with
// | three or more characters. It is one of the categories of the top level
// | domain (TLD) in the Domain Name System (DNS) maintained by the Internet
// | Assigned Numbers Authority.
// |
// | - [icannwiki](http://icannwiki.com/GTLD)

use {GtldKind, Gtld};

/// This is a list of all `Gtld` information is pulled from the following URL:
///
/// https://www.iana.org/domains/root/db
///
/// ### Examples
///
/// Retrieve all `Gtld`s in a `Vec`:
///
/// ```rust
/// let gtlds = gtld_data::all();
/// ```
// This list is automatically created by the `scripts/update.py` Python script.
// To update this list, run 'make update' and it'll automatically update.
// As this is auto-updated, the items in this list should not ever be manually
// modified. Even if there is a typo, etc., this should follow the IANA's
// website.
include!(concat!(env!("OUT_DIR"), "/gtlds.rs"));
