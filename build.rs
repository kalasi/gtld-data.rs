// ISC License (ISC)
//
// Copyright (c) 2016, Austin Hellyer <hello@austinhellyer.me>
//
// Permission to use, copy, modify, and/or distribute this software for any
// purpose with or without fee is hereby granted, provided that the above
// copyright notice and this permission notice appear in all copies.
//
// THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES
// WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF
// MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY
// SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES WHATSOEVER
// RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN ACTION OF
// CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF OR IN
// CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.
//
// What is a gtLD?
//
// | Generic Top Level Domain (gTLD) is an internet domain name extension with
// | three or more characters. It is one of the categories of the top level
// | domain (TLD) in the Domain Name System (DNS) maintained by the Internet
// | Assigned Numbers Authority.
// |
// | - [icannwiki](http://icannwiki.com/GTLD)

extern crate curl;
extern crate select;

use curl::http;
use select::document::Document;
use select::predicate::{Attr, Name};
use std::env;
use std::fs::File;
use std::io::Write;
use std::path::{PathBuf, Path};
use std::str;

static URI: &'static str = "https://www.iana.org/domains/root/db";

fn main() {
    let page_contents_bytes = http::handle()
        .get(URI)
        .exec()
        .expect(&format!("Error downloading URI: {}", URI)[..]);

    let page_contents: &str = str::from_utf8(page_contents_bytes.get_body())
        .expect("Error converting source from bytes to utf8");

    let document = Document::from(page_contents);

    let text: String = {
        let mut text: String = String::from(r#"
pub fn all<'a>() -> Vec<Gtld<'a>> {
    let mut list: Vec<Gtld> = Vec::new();

    // Include the generated `gtlds.rs` file. This is created later during the
    // build."#);

        let rows = document.find(Attr("id", "tld-table"))
            .find(Name("tbody"))
            .first()
            .unwrap()
            .find(Name("tr"));

        for row in rows.iter() {
            let cells = row.find(Name("td"));
            let mut iter = cells.iter();

            let domain_untrimmed: String = iter.next()
                .unwrap()
                .text()
                .replace(".", "")
                .replace("\n", "");
            let domain: &str = domain_untrimmed.trim();

            let k: String = iter.next()
                .unwrap()
                .text();

            let kind = match &k[..] {
                "generic" => "Generic",
                "country-code" => "CountryCode",
                "generic-restricted" => "GenericRestricted",
                "infrastructure" => "Infrastructure",
                "sponsored" => "Sponsored",
                "test" => "Test",
                _ => "Unknown gTLD type",
            };

            let organization = iter.next()
                .unwrap()
                .text()
                .replace("\n", " ")
                .replace("\"", "\\\"");

            text.push_str(&format!("
    list.push(Gtld {{
        domain: \"{}\",
        kind: GtldKind::{},
        organization: \"{}\",
    }});", domain, kind, organization)[..]);
        }

        text.push_str(r#"

    list
}"#);

        text
    };

    let gtld_path: PathBuf = Path::new(&env::var("OUT_DIR").unwrap())
        .join("gtlds.rs");

    let mut f: File = File::create(gtld_path.clone()).unwrap();

    f.write_all(text.as_bytes()).ok();
}
