extern crate gtld_data;

fn main() {
    // Check if a domain name exists via a `str`:
    let _exists = gtld_data::domain_exists("com");

    // Check if a domain name exists via a `String`:
    let _exists = gtld_data::domain_exists(String::from("com"));
}
