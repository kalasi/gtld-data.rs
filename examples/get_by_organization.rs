extern crate gtld_data;

fn main() {
    let organization: &str = "VeriSign Global Registry Services";

    // Retrieve when the organization name is a `str`:
    let _gtlds = gtld_data::get_by_organization(organization);

    // Retrieve when the organization name is a `String`:
    let name_as_string: String = String::from(organization);
    let _gtlds = gtld_data::get_by_organization(name_as_string);
}
