extern crate gtld_data;

use gtld_data::GtldKind;

fn main() {
    let _generics = gtld_data::get_by_kind(GtldKind::Generic);

    let _tests = gtld_data::get_by_kind(GtldKind::Test);
}
