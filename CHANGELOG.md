# Change Log
All notable changes to this project will be documented in this file.
This project adheres to [Semantic Versioning](http://semver.org/).


## [Unreleased]

### Added

### Changed


## [0.4.1] - 2016-07-05

### Added

### Changed

- modify the tests to account for a build-script
- simplify all examples
- add module-level documentation


## [0.4.0] - 2016-04-28

### Added

### Changed

The building of the list of gTLDs in the `all` function is now built on build of
the library, rather than being constantly updated in the source via Python.

This will save versions being released all the time, which should make things
simpler. The downside is this will cause the build client to need an internet
connection.


## [0.3.12] - 2016-04-26

### Added
- Added the following gTLDs: `mls`, `progressive`, `ಭಾರತ`, `ଭାରତ`, `ভাৰত`, `ഭാരതം`.
  All are gTLD types.

### Changed


## [0.3.11] - 2016-04-17

### Added
- Added the following gTLDs: `agakhan`, `akdn`, `ftr`, `imamat`, `ismaili`,
  `sbi`, `statebank`, `teva`, `网站`. All are `Generic` gTLD types.

### Changed


## [0.3.10] - 2016-04-06

### Added
- Added the following gTLDs: `baby`, `jnj`. All are `Generic` gTLD types.

### Changed


## [0.3.9] - 2016-04-05

### Added
- Added the following gTLDs: `abbvie`, `abudhabi`, `ابوظبي`. All are `Generic`
  gTLD types.


### Changed


## [0.3.8] - 2016-04-01

### Added
- Added the following gTLDs: `kpmg`, `northwesternmutual`, `vig`. All are
  `Generic` gTLD types.

### Changed


## [0.3.7] - 2016-03-29

### Added
- Added the following gTLDs: `htc`, `mutual`. All are `Generic` gTLD types.

### Changed
- Semver is now followed less strictly. All changes, removals, and modifications
  to gTLDs only modify the PATCH version. API changes are subject to Semver as
  usual.


## [0.3.6] - 2016-03-28

### Added
- Added the following gTLDS: `微博`, `sina`. All are `Generic` gTLD types.

### Changed


## [0.3.5] - 2016-03-25

### Added
- Added the following gTLDs: `anquan`, `jcp`, `nissay`, `shouji`, `weibo`,
  `xihuan`, and `yun`. All are `Generic` gTLD types.

### Changed
- Changed the `organization` of the gTLDs `aws`, `talk`, `家電`, and `you` from
  `Amazon EU S.à r.l.` to `Amazon Registry Services, Inc.`.


## [0.3.4] - 2016-03-24

### Added
- Added the following gTLDs: `aws`, `talk`, `家電`, `you`. All are `Generic`
  gTLD types.

### Changed


## [0.3.3] - 2016-03-23

### Added

- Added the following gTLDs: `ally`, `barefoot`, `extraspace`. All are `Generic`
  gTLD types.

### Changed


## [0.3.2] - 2016-03-16

### Added

- Added the gTLD `stream`, a `Generic` type.

### Changed


## [0.3.1] - 2016-03-15

### Added

- Added the following gTLDs: `gallo`, `shaw`. All are `Generic` gTLD types.

### Changed


## [0.3.0] - 2016-03-09

### Added

### Changed

- Changed the name of the country code `.cx`'s organization from
  "Christmas Island Internet Administration Limited" to
  "Christmas Island Domain Administration Limited"

## [0.2.4] - 2016-03-07

### Added

- Added the following gTLDs: `avianca`, `bcg`, `gmbh`, `locus`. All are
  `Generic` gTLD types.

### Changed

## [0.2.3] - 2016-03-06

### Added

- Added the gTLD `бг`, a `CountryCode` type.

### Changed


## [0.2.2]

### Added

- Added the following gTLDs: `kerryhotels`, `kerrylogistics`, `kerryproperties`,
  `kuokgroup`, `passagens`, `vuelos`, and `嘉里大酒店`. All are of a `Generic`
  gTLD type.

### Changed


## [0.2.1]

### Added

### Changed

- Modified keywords in the `Cargo.toml` file.


## [0.2.0] - 2016-02-28

### Added

### Changed

- Renamed crate from `gtlds` to `gtld_data`, as this crate does not
  involve working to an amount with gTLDs themselves, but rather just provides
  a list of the gTLDs.


## [0.1.0] - 2016-02-28

Initial commit.

[0.4.1]: https://github.com/taiyaeix/gtld-data.rs/compare/v0.4.0...v0.4.1
[0.4.0]: https://github.com/taiyaeix/gtld-data.rs/compare/v0.3.12...v0.4.0
[0.3.12]: https://github.com/taiyaeix/gtld-data.rs/compare/v0.3.11...v0.3.12
[0.3.11]: https://github.com/taiyaeix/gtld-data.rs/compare/v0.3.7...v0.3.11
[0.3.7]: https://github.com/taiyaeix/gtld-data.rs/compare/v0.3.2...v0.3.7
[0.3.2]: https://github.com/taiyaeix/gtld-data.rs/compare/v0.3.1...v0.3.2
[0.3.1]: https://github.com/taiyaeix/gtld-data.rs/compare/v0.3.0...v0.3.1
[0.3.0]: https://github.com/taiyaeix/gtld-data.rs/compare/v0.2.4...v0.3.0
[0.2.4]: https://github.com/taiyaeix/gtld-data.rs/compare/v0.2.3...v0.2.4
[0.2.3]: https://github.com/taiyaeix/gtld-data.rs/compare/v0.2.2...v0.2.3
[0.2.2]: https://github.com/taiyaeix/gtld-data.rs/compare/v0.2.1...v0.2.2
[0.2.1]: https://github.com/taiyaeix/gtld-data.rs/compare/v0.2.0...v0.2.1
[0.2.0]: https://github.com/taiyaeix/gtld-data.rs/compare/v0.1.0...v0.2.0
[Unreleased]: https://github.com/taiyaeix/gtld-data.rs/compare/v0.1.0...HEAD
